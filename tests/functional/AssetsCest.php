<?php namespace App\Tests;
use App\Tests\FunctionalTester;
use Codeception\Module;
use Codeception\Util\Locator;

class AssetsCest
{
    public function _before(FunctionalTester $I)
    {
    }

    // tests
    public function loadCssFileInHead(FunctionalTester $I)
    {
        $I->amOnPage("/");

        $I->expect("a app.css file is loaded in <head>");
        $I->assertContains('/build/app.css', $I->grabAttributeFrom("head > link", "href"));
    }

    public function loadJsFileInBody(FunctionalTester $I)
    {
        $I->amOnPage("/");

        $I->expect("a app.js file is loaded in <body>");
        $scriptTags = $I->grabMultiple("body script", "src");
        $I->assertNotEmpty($scriptTags);
        $I->assertContains('/build/app.js', $scriptTags);
    }
}
