<?php

namespace App\Tests;

use App\Tests\AcceptanceTester;

class HomepageCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    // tests
    public function seeWelcomeMessage(AcceptanceTester $I)
    {
        $I->amOnPage("/");

        $I->seeInTitle("Home | RecipeManagement");
        $I->see("Welcome to RecipeManagement");
    }
}
